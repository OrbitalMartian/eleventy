const q = document.querySelector.bind(document);
const qA = document.querySelectorAll.bind(document);

async function loadComments(link) {
    q("#comments button").innerText = "Loading…";

    const popup = document.getElementById("popup");
    const closeButton = document.getElementById("close-popup");

    let css_head = document.getElementsByTagName('HEAD')[0]
    let css_link = document.createElement('link');
    css_link.rel = 'stylesheet';
    css_link.type = 'text/css';
    css_link.href = '/css/comments.scss';

    // Append link element to HTML head
    css_head.appendChild(css_link);

    let post = {
        id: link.split('/').slice(-1)[0],
        instance: link.split('/')[2]
    };
    const response = await fetch(`https://${post.instance}/api/v1/statuses/${post.id}/context`);
    if (response.status !== 200) {
        q("#comments button").innerText = "Error!";
    }
    const replies = (await response.json()).descendants;
    const html = replies
        .map(reply => {
            return `
				<article class="comment">
				<div class="comment_user">
					<img class="avatar" src="${reply.account.avatar_static}#shadow" alt="" style="width: 25%; height: 25%;">
					<div class="comment__details">
						<a href="${reply.account.url}" class="comment__author__username">${reply.account.username}@${reply.account.url.split('/')[2]}</a> <span clas="comment__time">${reply.created_at}</span>
					</div>
				</div>
					<div class="comment__content">
						${DOMPurify.sanitize(reply.content)}
					</div>
				</article>
			`
        })
        .join('');
    q("#comments").innerHTML = html;
}
