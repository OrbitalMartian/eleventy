---
layout: layouts/base.njk
permalink: /subscribe.html
eleventyNavigation:
  key: Home
  order: 1
---
# Subscribe
*Follow my posts all over the internet, from the old fashioned RSS feed to the mighty Fediverse.*


## RSS
Subscribe to my RSS feed and see all my fresh, new posts in your feed reader.

<nav>
<div class="button-container">
    <a href="/feed.xml">Subscribe via RSS</a>
</div>
</nav>

## Fediverse
Follow my main Fediverse account and get all my microbloggin posts straight in your Fedifeed. Whether you use Mastodon, Pleroma, *key, GoToSocial or any other ActivityPub enabled software, you can follow me.

<nav>
<div class="button-container">
    <a href="https://alpha.polymaths.social/@orbitalmartian">Fedi Follow</a>
</div>
</nav>

## Videos
I have two places for people to view my video content - Peertube which has both ActivityPub following support and RSS support, and YouTube which you can subscribe on YouTube itself or use RSS (**RECCOMENDED**).

<nav>
<div class="button-container-grid">
    <a href="https://peertube.linuxrocks.online/@orbitalmartian8">Peertube Follow</a>
</div>
<div class="button-container-grid">
    <a href="https://peertube.linuxrocks.online/feeds/videos.atom?videoChannelId=5811">Peertube RSS Follow</a>
</div>
<div class="button-container-grid">
    <a href="https://youtube.com/@orbitalmartian8">YouTube Subscribe</a>
</div>
</nav>

## Newsletter
I am hereby retiring the option for the ButtonDown newsletter, as with no subscribers and me not using it, it's a waste of an option.
<form
  action=""
  method=""
  target=""
  onsubmit=""
>
  <label for="bd-email">Enter your email</label>
  <input type="email" name="email" id="bd-email" />
  
  <input type="submit" value="Subscribe" />
  <p>
    <a href="" target="_blank">Powered by Buttondown.</a>
  </p>
</form>