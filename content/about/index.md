---
layout: layouts/base.njk
eleventyNavigation:
  key: About
  order: 3
---
# About

Hey folks! I'm CJ, known online as OrbitalMartian.
I am OrbitalMartian, a dyslexic artist, programmer, gamer and content creator. I advocate for FOSS projects and use many myself as well as maintain a couple of personal projects too.
I have an interest in aviation and aircraft.

[Fly Free, Fly FlightGear](https://flightgear.org)

<a href="https://vivaldi.com?pk_campaign=Banners&pk_kwd=pu180x50"><img src="https://vivaldi.com/buttons/files/150x50_2.png" alt="Get Vivaldi Browser" style="border:0"></a>

<a href=""><img src="/img/Written-By-Human-Not-By-AI-Badge-black.png" alt="Not Written By AI badge"></a>

<p>👻 Proud member of <a href="https://darktheme.club/">darktheme.club</a> 👻</p>