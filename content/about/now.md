---
title: Now
layout: layouts/base.njk
permalink: /now/
---

Here is what I'm up to right now.

**Last updated: 17th May 2024 @ 20:20**

## Me
- Currently I am living in the UK.
- I'm working within a school in admin.
---
## What I'm Reading
- Currently I'm reading The Hitchhikers Guide To The Galaxy by Douglas Adams.
- I'm enjoying a lot of blogs, mostly small web people - links available in my [blogroll](/blogroll.html).
---
## What I'm Watching
- I'm addicted to The Big Bang Theory (I've got the full show on DVDs).
- I have recently started watching Young Sheldon on Channel4's My4 (guess I have a universal addiction for the whole TBBT franchise, including spinoffs).
---
## What I'm Working On
- Right now, I'm working on my blog.
- I've recently fallen in love with making liveries for FlightGear. [Take a look at my finished releases here on my site.](/fglivs/).
---
## What I'm Listening To
- I've been getting back into listening to podcasts, recently been enjoying 'Status: Untraced', a true crime podcast about the disappearance of a hiker in India.
<!-- - I'm not currently listening to any music, nor any podcasts. -->
---
## What I'm Writing
- I'm currently writing blog posts towards my first attempt at 100DaysToOffload.
- I'm also continuing work on my NaNoWriMo23 novel named Lunar Voyager.
- I'm writing a mini story series titled `A Race Against Time`. [READ HERE](https://orbitalmartian.codeberg.page/tags/a-race-against-time/)!

---

## What I'm Using
### Desktop:
#### Main:
- Operating System - Windows 11
- Browser - Vivaldi / Florp
- Art software - Krita / LibreSprite
#### Gaming:
- Operating System - Windows 11
- Browser - Vivaldi / Florp
- Favourite game - FlightGear
- Headtracking software - Opentrack
- Flight sim companion - LittleNavMap
### Mobile:
#### Phone:
- Model - iPhone 14
- Operating System - iOS 17.5
- Browser -  Vivaldi / Firefox
- Fediverse client - Enafore / IceCubes
- Matrix client - Element for iOS
- XMPP client - Monal
- Special App - iSH
#### iPad
- Model - iPad 10<sup>th</sup> Generation
- Operating System - iPadOS 17.4
- Browser -  Vivaldi / Firefox
- Art software - Procreate / Sketchbook / Pixen
- Fediverse client - Enafore / IceCubes
- Matrix client - Element
- XMPP client - Monal



