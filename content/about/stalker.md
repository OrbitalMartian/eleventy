---
layout: layouts/base.njk
eleventyNavigation:
  key: Stalker
  order: 3
---

# Stalker
Here is a list of my social links for all my stalkers :) Follow whichever you want.

## Main Accounts
* 🌎 [GoToSocial](https://alpha.polymaths.social/@orbitalmartian)
* 🌎 [LinuxRocks Mastodon](https://linuxrocks.online/@orbitalmartian)
* 🌎 [Akomma Alt](https://pleroma.envs.net/orbitalmartian)
* 🌎 [Shonk Alt](https://shonk.phite.ro/@OrbitalMartian)
* 🌎 [Peertube](https://peertube.linuxrocks.online/c/orbitalmartian8/videos/)
* 🔒 [YouTube](https://youtube.com/@orbitalmartian)
* 🌎 [Bookrastinating (Bookwyrm)](https://bookrastinating.com/user/OrbitalMartian)
* 🌎 Matrix User - orbitalmartian:envs.net

## Secondary Accounts
* 🌎 [Social Linux Pizza Alt](https://social.linux.pizza/@orbitalmartian)
* 🌎 [Vivaldi Social Alt](https://social.vivaldi.net/@charl8)
* 🌎 [Treehouse Systems Alt](https://social.treehouse.systems/@orbitalmartian)
* 🌎 [Plasmatrap Alt](https://plasmatrap.com/@OrbitalMartian)
* 🌎 [Dot Art Writeas](https://dotart.blog/orbitalmartian/)