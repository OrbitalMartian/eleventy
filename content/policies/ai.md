---
title: AI Policy
layout: layouts/home.njk
permalink: /policies/ai.html
eleventyExcludeFromCollections: true
---

<p class="notice">This policy is still being written and is a WIP. Please bear with me whilst I work to complete it.</p>

On my various social medias, I have, since the rise of AI popularity, voiced my opinion towards AI. I mostly (like 95%) dislike it, there are some uses which I agree can be useful but majority aren't. I'm also fully against AI image generation.

This policy sets out my views and sets expectations for this blog to follow regarding AI and the Use Of.

---

AI, in this case, is defined as large language models and image generators (such as GPT, Gemini, Dall-E and Midjourney) or anything that is marketed as "AI". My posts, writing, artwork and all other content on this site, will be AI free, except from certain, specific scenarios but these will be marked clearly in some shape or form.

It is my belief that as a personal blog, the site should be my own and not the work of LLMs.