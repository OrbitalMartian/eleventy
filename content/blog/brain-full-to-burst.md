---
title: My Brain Is Full To Bursting
date: 2024-05-31
tags:
- Ramblings
- 100DaysToOffload
draft: false
---

<aside>
<p>This blost focuses on some topics that some people might find distressing or upsetting.</p>
</aside>

Last night, as I was trying to fall asleep, and my mind was going all over the place, from wanting to learn to draw, to wanting to learn bash and Python, to my family cat (who we sadly lost over 10 years ago), to writing all the things I want to, to learning a musical instrument or a few. Things are really hitting me at the moment, my brain just feels too full to cope. Over the weekend, I'll be stepping back a little bit off of social networks (Discord, Fediverse, etc) and try to recalibrate myself, calm my brain, make a todo list and actually manage my thoughts, which hopefully my new way of writing will fix.

And with my birthday coming up, a week tomorrow, I want to be nice and chilled out by then.

Hopefully this hasn't been too tough, it took me all day to write, my brain is just too full **(as this post is about)**.

Enjoy this post and my new [A Race Against Time site](https://arat.codeberg.page).

---

This is post 59/100 for [#100DaysToOffload](https://100daystooffload.org).
