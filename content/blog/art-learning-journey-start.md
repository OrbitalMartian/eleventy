---
title: The (Re) Start Of My Art Learning Journey
date: 2023-12-02
draft: true
---

## Scribble scribble

I have been trying to learn to draw / paint for approximately 2-5 (two to five) years, and about a year ago I got a base iPad 19th Gen with an Apple Pencil 1st Gen, as well as purchasing [Procreate](https://procreate.com). Thing have been slow for my learning, I started whilst in school, and had no time to actually commit, what with having coursework to do. Now I have finished school and have a job, I'll be able to commit some time towards learning / teaching myself. 

Today I read a [blog post](https://www.davidrevoy.com/article1007/my-brushstrokes-against-ai-art) from my favourite artist [David Revoy](https://davidrevoy.org), which was focused on AI art generation, but really took my mind to thinking back about learning to draw / paint. This made me think about what style I'd like to learn more towards, of course I will have to keep an open mind and be open to trying other styles.



<details>
  <summary>Click here to expand my plans</summary>
 My plan is to...
</details>