---
title: I Made My Own Client (Kinda)
date: 2024-05-19
tags:
- Fediverse
- Programming
- Webdev
- 100DaysToOffload
draft: false
---

I have used a variety of Fediverse clients over my years on the Fedi (yes it’s nearly/over 2 years, I can’t find my original account, seems Fosstodon has deleted it :( ). When I first joined, I used the Mastodon, then Pleroma, then back to Mastodon web UI; I never knew that third party clients were a thing. Now I’m an experienced Fedizen, I have tried majority of the web based apps and a couple of iOS apps. The ones that I kept going back to were Semaphore and then (once I discovered it) Enafore.

## Why Enafore?
Well, Enafore allows you to choose which text formatting you want to use, which, as GoToSocial supports MarkDown, I use to get MarkDown formatting in my toots. It’s a nice smooth app with a variety of themes, which are mostly great (there are a couple which I don’t like, but it might just be me).

Alongside the themes and MarkDown support, there are some lovely Wellness settings such as removing notification number and other statistics like that which could lead to Social Networking addiction.

## Why I Made My Own (Kinda)
Let’s start with the “(Kinda)” part, I have got my own client, but it is a fork of [Enafore](https://enafore.social). I decided to try it out and make some changes, such as custom themes and changing “Post” to “Toot”. Toot is the original word that Mastodon used before they found it’s true meaning. I prefer Toot over Post, as it seems, do others on the Fediverse.

Hope you enjoyed reading, catch you all later.

---

This is post 56/100 for [#100DaysToOffload](https://100daystooffload.com).