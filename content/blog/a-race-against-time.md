---
title: 51 - A Race Against Time - Episode 1
date: 2024-04-28
tags:
- Writing
- 100DaysToOffload
- ARaceAgainstTime
draft: false
---

<aside>
<h3>Randomly generated prompt</h3>
Your character is young, outgoing and friendly. The story begins at a market. Someone is tormented by the memory of a dead family member. The theme is: a race against time.
</aside>

The bussling market street was jam packed with vendors and the public alike, but this was no ordinary market, no no. This market was out of this world (*rather quite literally*). A group of youths strolled down, looking from left to right at the diverse stalls. In the middle of the group, an orange skinned alien chatted and laughed with their friends. The tallest of the group, he was very friendly with everyone they met, interacting with everyone and everything they met. His name was 08-AD (although their friends called them Dashad). Approaching the end of the street, the group bid farewell and went their separate ways. Dashad walked down a saide alley, moving away from the crowded routes. Little did they know, there was a portal hidden behind one of the doors. They slowed their stroll and turned to face one of the many doors along the alley. Opening the door, they were blinded by a bright, purple light. Dashad cautiously stepped through the doorway and disappeared through the portal.

On the other side, they were face to face with a transparent, floating entity. Dashad's face went a grey as they recognised the face of the ghost. It was their grandfather, to whom they were very close. He said "08-AD, you must look further than the wall, further than anyone has ever been. There you will find the truth, the truth behind my death and to save what I worked so hard to create and protect.". Then he slowly faded away, a tear rolled down Dashad's face. He turned to face the wall behind them, as their brain spun and slowly a small lightbulb idea occured to them.

