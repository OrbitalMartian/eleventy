---
title: Christmas Break With No Posts
description: No more blog posts from me until the New Year
date: 2023-12-17
tags:
- Christmas
- PSA
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/111535218064450203
---

Good evening folks, welcome to another blog post, this is a quick PSA (Public Service Announcement).

I will not be posting any more scheduled posts until the New Year.

So until then, have a Merry Christmas and hapoy New Year!
