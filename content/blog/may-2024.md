---
title: May 2024
date: 2024-06-01
tags:
- Month Review
- 100DaysToOffload
draft: false
---

May was a long one for me personally, I had a critical breakdown in the last couple of days of the month, which I mentioned in my last blost. However, ot was also full of some great things! From new podcasts being listened to and more reading being done.

## Podcasts
This month, I started listening to [Wolf 359](https://wolf359.fm), a sci-fi podcast, which I have now enjoyed the first 31 episodes which I have thoroughly enjoyed.

## Reading
I'm **still** on A Hitchhikers Guide To The Galaxy, but I am now 120 pages into it. Still enjoying it, but I can't wait to finish it so I can move on to the next one.

## Gaming
I have again been playing a lot of [FlightGear](https://flightgear.org/), flying a lot of helicopters and GA aircraft, with a dabble of famous VTOL jet. I also touch Roblox (and instantly regretted it) and played a bit of Enlisted (Gaujin's World War 2 game).

## Writing
I have written 7 blosts this year which I feel is great.

And that's all for the month for me, I hope that June will be a better one, with my birthday a week from today, I have some plans for a couple of fun events. Have a good one!

---

This is post 60/100 for [#100DaysToOffload](https://100daystooffload.org).
