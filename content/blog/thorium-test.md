---
title: 47 - Testing Thorium Out
description: I tested the Thorium browser out and I quite enjoyed it.
date: 2024-04-22T18:00:00+01:00
tags:
- Browsers
- Reviews
- Thorium
- 100DaysToOffload
draft: false
---

I have recently been using [Florp](https://floorp.app/en/) and [Firefox](https://www.mozilla.org/en-GB/firefox/new/) mobile and have enjoyed it, but I have always preferred [Chromium](https://chromium.org) based browsers, coming from using [Vivaldi]( https://vivaldi.org). I have heard about the [Thorium](https://thorium.rocks) browser and decided to try it out.

Thorium features numerous behind the scenes tweaks which make Thoriumrun much faster than vanilla Chromium. I haven't got too much else to say about the browser. My one issue is that I can't enable Picture-in-Picture mode which I use for Channel 4's My4 whilst flying in the sim, but I'm sure this can be fixed with an extension.

Other than that, it's a great browser. Much better than Google Chrome and vanilla Chromium.

That's it for this post, hope you enjoy (I'll try to write some more fleshed out posts soon but for now there will be short, snappy ones). This post is post 47 of my first attempt at [#100DaysToOffload](https://100daystooffload.com).