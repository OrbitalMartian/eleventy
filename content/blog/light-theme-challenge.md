---
title: 48 - I'm Using Light Theme For A Month
description: I'm embarking on a challenge to use just light theme for a month.
date: 2024-04-22T22:00:00+01:00
tags:
- Light Theme
- Challenges
- 100DaysToOffload
draft: false
---

After seeing Matt's ([TLC](https://thelinuxcast.org)) [Fediverse post](https://fosstodon.org/@thelinuxcast/112316005529721399) about a challenge to use light theme for a month, I decided that I will too. 

From today (22nd April 2024) I will use just light theme until 22nd May 2024; unless whatever I'm using doesn't provide a light theme (the horror).

In the coming days I'll make a little countdown timery thing so that the challenge can be tracked but for now that's it. Maybe you too shall join the Light Side with this challenge.