---
title: Episode 1 - The Potion of Flight 
description: Episode 1 of a fan written mini novel based on the FLOSS webcomic Pepper and Carrot
date: 2023-12-08
tags:
- Pepper & Carrot
- Writing
draft: true
---

A dark room, with a large cauldron sat in the centre. A young girl stood holding a box of white and gold powder. Pouring it into the golden, yellowy bubbling cauldron, the girl, whose name is Pepper, said "...and the last touch". Her golden cat was sat on the countertop behind her, bored looking, staring between her and a wooden broomstick with a label attached. The label said "18".
