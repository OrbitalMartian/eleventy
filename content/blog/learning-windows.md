---
title: 49 - I Want To Learn Windows Better
description: As my primary desktop OS is sadly Windows, I want to learn more about it and how to utilise all features to there full potential.
date: 2024-04-23
tags:
- Windows
- Windows 11
- 100DaysToOffload
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/112322050673472357
draft: false
---

I am mostly operating on my phone and iPad which is great but some things need to be done on a desktop. My main desktop OS is, sadly, Windows 11, and there is a lot of cool stuff. To get better with my system and to best utilise it to it's full potential, I want to learn all I can.

I have initial plans to learn Powershell and other cool things like that. Other things like shortcuts, etc will also be useful to learn as well as Batch scripting.

Plans are still in my brain at the moment, so nothing will happen anytime soon, but I'll be working on fleshing out plans and getting started.

Hope you enjoyed, feel free to share your thoughts in email or through this [Fediverse thread](https://alpha.polymaths.social/@orbitalmartian/statuses/01HW64Y3AQFF96W91A1ZM1CKGT).


