---
title: 50 - My ISP's Tech Support Sucks
description: I had to call my ISP's support line, and no surprise it sucked.
date: 2024-04-24
tags:
- Ramblings
- Networking
- 100DaysToOffload
draft: false
mastodonThread: https://linuxrocks.online/@orbitalmartian@alpha.polymaths.social/112328406517054893
---


After having numerous issues with my router, we had an Openereach engineer come out yesterday to investigate further. They found a fault inside the house (a loose wire inside a disused network port) and a fault outside the house (I never found out what it was). They tested with a brick machine thingy madjig and he was getting 70 mbps going into the router, whereas my mum's laptop was reading 40 mbps using [Speedtest by Ookla](https://speedtest.net). The engineer proclaimed that the router was faulty, so we called our ISP and they sent a new one which arrived this morning.

After I finished work, I got to work setting this up. I unplugged the old router from the mains then removed all the network and telephone lines. Passing this to my dad who put it into a cardboard box on the other side of the room. We got the new router and plugged everything back in and it set itself up. We tried to connect our phones to the new router but the new name didn't show up in the list, but the old one did, and if I connected to it, I got internet connection and it worked. I got so confused I thought the old router was somehow still connected or something was wrong.

So we gave our ISP's tech support line a call and we spoke with a lady for 53 minutes where she told us to do everything that we had already done. After dumbing the issue down a bit, she then told us that it was by design that the old name and password transfer to the new router to reduce hassle (oh how stupid of an idea that is, who tf designed this). This was about 45 minutes into the call. At about 50 minutes in, I was steps ahead of her guidance to change the WiFi name and password to the new one (as stated on the card which came with the router, and on the router itself). So after that was all done, there was nothing else to do so we ended the call.

At one point she said, (along the lines of), "I'll need to get my manager involved to help with this", I then heard her typing on a keyboard (clearly messaging on Teams or Slack) and then 3 or so minutes later she came back with the answer of the name is designed to not be the one on the router. Working from home clearly and as seen first hand, they can't easily communicate with their colleagues. I work in an office answering the phone, etc, and if I need help, I can physically talk to my manager and colleagues and get the help and answers immediatly without the hassle of messaging. She also kept saying about out minimum guaranteed speed and that what we are getting (67 down, 18 up) is pretty good, but the actual Openreach engineer said it should be higher, so who knows, but we got a message earlier to say there was an issue with the performance of our network, and they were working on fixing it.

Anyway, I have got it fixed now, but wanted to share my experience with my ISP. Let me know your thoughts in email or in the Fediverse thread below.



