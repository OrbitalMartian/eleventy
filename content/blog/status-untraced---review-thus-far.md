---
title: "Status: Untraced - Review Thus Far"
date: 2024-05-10
tags:
- Podcast Reviews
- Reviews
- 100DaysToOffload
draft: false
---

Last week, I found a new podcast after creating an account on Podbay (which is now where I find my podcasts). A new genre to me, this true crime podcast, `Status: Untraced`, has got me interested in this genre, and I have caught this series as it's being made (episode 5 was released on the 8th of this month so that is amazing). I've listened to Episode 1, Episode 2, and a bonus episode between 2 and 3 (well only half of the bonus so far). I am hoping to get back into listening to podcasts more and I hope this new app will help with that.

That's all I'm going to say on it for now, so enjoy a nice summer's evening (or whatever your timezone is), I'll see you in the next one.

---

This is post 55/100 for [#100DaysToOffload](https://100daystooffload.com).

This post is also cross posted on my [Envs.net BashBlog blog](https://orbitalmartian.envs.net/blog)

