---
title: April 2024
description: April 2024 had a lot of ups, here's my review on the month.
date: 2024-05-06
tags:
- Month Review
- 100DaysToOffload
draft: false
---

I have seen some people doing this kind of post for a few months now, so I thought I'd give it a go. As this is my first one, please don't worry if things don't look as good as other people's or if I don't write it as well as I could. I am of course a few days late with this but it's not a big deal.

## What I Read#
Throughout the month, I have tried my very best to read more, I finished reading Alex Rider: Eagle Strike and then started reading Hitchhiker's Guide To The Galaxy, which I am 65 pages through and am thoroughly enjoying. I have however been neglecting my [Bookwyrm account](https://bookrastinating.com/@orbitalmartian) which I really need to change. I have read many blog posts throughout the month, none of which I can remember at this moment in time, but there were some good ones.

## What I Wrote
In April, I started on a journey writing a mini story series called A Race Against Time. Things on Lunar Voyager has stalled, but I still have a little bit of my heart and brain stuck in that story. I've written many blog posts in April, all for my attempt at [#100DaysToOffload](https://100daystooffload.com).

## Listening
I finally got back into listening to my backlogged podcast "To Listen" list. With a couple of [The Linux Cast](https://thelinuxcast.org) episodes and a couple of [The WAN Show](https://podbay.fm/p/the-wan-show-podcast) episodes. I also enjoyed listening to [RadioFreeFedi](https://radiofreefedi.net) towards the end of the month.

## Device Usage
### Phone
I used my phone for about 5 or so hours a day each day in the month, I don't have a way to see exactly how long I spent on my phone each day, week, month.

### Laptop
I enjoyed using my main laptop to do some writing, including the start of A Race Against Time. I also browsed the Fediverse from my laptop.
On the weekends, I pleasure myself with gaming, I only played FlightGear in April but other games are in my library, I swear.


Well, that's it after a good few days of writing, this took way too long XD. See you in the next one.

Let me know how you keep track of device usage, what podcast app you use and what games you've been playing - either in email or on Fediverse.

---

This is Post 54/100 for [#100DaysToOffload](https://100daystooffload.com).
