---
title: My Random Brain Farts
date: 2024-05-03
tags:
- 100DaysToOffload
- Ramblings
draft: false
---

I'm tired, so here are the mad ramblings of a sleepy martian, waiting for his sleeping pod to automatically open for bed.

I have finished the work week, as of a few hours ago, so kicked off the weekend with some writing for my April month review. I haven't finished and it'll probably be a little while before I finish it.

I pick up my new glasses tomorrow so I will likely have some Fediverse toots about how clearly I can see once I get them. However, it's not the first time I have had glasses, I've had them for the last 5 or so years.

Anyway, that's me for the night. I'll leave it there and enjoy my well earned rest.

---

This is post 53/100 for [#100DaysToOffload](https://100daystooffload.com).




