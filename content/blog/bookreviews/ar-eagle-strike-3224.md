---
title: 31 - Alex Rider Eagle Strike - Book Review - 3/2/24
date: 2024-02-03
tags:
 - Book Reviews
 - 100DaysToOffload
---

I'm currently reading Ravens Gate by Anthony Horowitz.

### Pages: 21/352

Past the prologue and into chapter one. Enjoying it, and I'm glad to be back to the genres that I am more comfortable with (mystery, adventure, action).

The Lord Of the Rings trilogy is on my list to read but I only have book two and three, I need to order book one but keep forgetting to.

[View this progress review on Bookwyrm](https://bookwyrm.social/user/OrbitalMartian/comment/3753817)

<a href="https://100daystooffload.com" class="--center"><button>#100DaysToOffload</button></a>