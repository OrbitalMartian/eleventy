---
title: Reading Slower Than A Snail
description: My reading has been very slow lately, here's a quick rundown.
date: 2024-05-26
tags:
- Reading
- Books
- 100DaysToOffload
draft: false
---

At the start of 2024, I set myself a goal to finish 5 books in 2024, I can confirm I've finished 1 in month 5 (May). Last month (on 8th April), I started reading Hitchhikers Guide To The Galaxy, which as of today, I have read up to page 89. I have been slowly reading this year, and I feel bad. I have always said I need to read more, but it's finding the time to go off to a quiet corner of the house and read.

## What Can I Do To Fix It?
Well, tbh, I don't really know (kinda the point of this post, a cry for help). I could put limits on my phone and read more, I could also leave my family and read more, but my main issue is, I can't read if there is voices or talking going on somewhere that I can hear, it messes with my brain and merges them, hence why I can't read whilst with my family. - If you know of a solution, please do let me know, either Fedi or email, both are acceptable, even Matrix (if you so desire).

## My Short Term Game Plan
In the long run, this is going to be a long term problem that will need a big fix, which isn't great for the short term (especially when I'm not sure what the answer is). So, in the short term, I will try my hardest to read when I get the chance, when there is a quiet minute, sit on my beanbag and open my book; it seems to be forced but it might push me to read more than being on my phone or playing video games

I'm still not too sure on the fix for this large issue, so if you have any suggestions, please hit me up either on Fedi or in email.

---

This is post 57/100 for [#100DaysToOffload](https://100daystooffload.org/).