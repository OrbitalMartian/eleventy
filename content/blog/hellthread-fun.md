---
title: Polymaths Hellthread - Fun Notifications
description: I explore the landscape of hellthreads on the Fediverse.
date: 2024-04-30
tags:
- 100DaysToOffload
- Fediverse
- Hellthreads
draft: false
---

I recently (though my notifications disagree) was added to a hellthread on the Fediverse, which was rooted from a single comment about catching up with Fediverse toots. Well they got what they wanted, with pretty much all the members of Polymaths and some of our ~~traitors~~ sorry fellow FediFriends, though we on Polymaths are at this point the only ones who can send messages as there are so many usernames to be pinged (our 5000 character limit is really paying off, sucks to be on Mastodon with your 500 or less limit XD).

If you want to see the [original toot](https://alpha.polymaths.social/@roguefoam/statuses/01HWNH3XQKWR16W409MDRBMEFW$0), see the link.

I enjoyed it, my notifications have had a battering as it seems to have been ages but it's only 24 hours, it's pretty much feed of it's own.

Anyway, that concludes post 52/100 for [#100DaysToOffload](https://100daystooffload.org). Enjoy!



